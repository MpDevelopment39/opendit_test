# Test OpenDit (Login)

<p align="justify">
  Este repositorio contiene el código del proyecto <strong>Opendit Test</strong>.
  
  Para los comentarios del código utilizo la extensión Better Comments de VS Code.

  Para presentar la propuesta solicitada se han utilizado las siguientes librerías:
  <br>
<h2>Librerías utilizadas para el diseño</h2>

  * [<b>sizer</b>](https://pub.dev/packages/sizer)
    * Para realizar diseño responsive en función de la resolución de cada dispositivo.
  * [<b>lottie</b>](https://pub.dev/packages/lottie)
    * Para mostrar animaciones en formato json.
  * [<b>top_snackbar_flutter</b>](https://pub.dev/packages/top_snackbar_flutter)
    * Para mostrar snackbars en la parte superior de la pantalla.Se ha hecho un diseño personalizado para respetar el diseño del proyecto.
  * [<b>flutter_svg</b>](https://pub.dev/packages/flutter_svg)
    * Para poder utilizar recursos vectoriales en formato svg.
  * [<b>animate_do</b>](https://pub.dev/packages/animate_do)
    * Para utilizar animaciones en el diseño.

<h2>Librerías utilizadas para funcionalidades</h2>

  * [<b>provider</b>](https://pub.dev/packages/provider)
    * Como manejador de estado.
  * [<b>http</b>](https://pub.dev/packages/http)
    * Para las peticiones al Servicio Rest.
  * [<b>connectivity_plus</b>](https://pub.dev/packages/connectivity_plus)
    * Para manejar los cambios de conexión en tiempo real o ver el estado de red del dispositivo en un momento puntual.

Para la internacionalización he decidido usar el sistema propuesto por Flutter (flutter_localizations) junto con (intl) que aplica el idioma según el utilizado por el SO. He utilizado este formato ya que no quería modificar el diseño para implementar la posibilidad de que el usuario lo cambiase de forma manual a través de interfaz y así implementar las traducciones a través de providers y un mapeo de las mismas.


A continuación se muestra el resultado del proyecto:

</p>
<p align="center">
    <img src="/assets/readme/gif1.gif" width="310" height="640">
    <img src="/assets/readme/gif2.gif" width="310" height="640">
    <img src="/assets/readme/gif3.gif" width="310" height="640">
    <img src="/assets/readme/gif4.gif" width="310" height="640">
</p>
