import 'package:flutter_test/flutter_test.dart';
import 'package:opendit_test/utils/utils.dart';

void main(){

  //* Tests para comprobar diferentes casos del método ValidatePassword()
  group('ValidatePassword()', (){
    test('La contraseña no es valida',(){
      bool result = Utils.validatePassword('');
      bool result1 = Utils.validatePassword('aa');
      bool result2 = Utils.validatePassword('passwordToValidate');
      bool result3 = Utils.validatePassword('12345678910');
      bool result4 = Utils.validatePassword('aa123456');
      bool result5 = Utils.validatePassword('aa123456#');
      expect(result && result1 && result2 && result3 && result4 && result5 ,false);
    });

    test('La contraseña es valida',(){
      bool result = Utils.validatePassword('aa123456#A');
      bool result1 = Utils.validatePassword('Bb34567@');
      bool result2 = Utils.validatePassword('&a123456H');
      bool result3 = Utils.validatePassword('AAaabb11!');
      expect(result && result1 && result2 && result3,true);
    });
    
  });

}