//* Clase modelo para manejar la respuesta actual obtenida por la petición de Login a la Api
class LoginResponseModel {

  String? error;
  String? token;

  LoginResponseModel({this.error, this.token});

  LoginResponseModel.fromJson(Map<String, dynamic> json) {
    if (json.containsKey('error')) error = json['error'];
    if (json.containsKey('token')) token = json['token'];
  }

}