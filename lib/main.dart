import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:opendit_test/pages/login/login_page.dart';
import 'package:opendit_test/providers/login_provider.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
void main() {

  //* Proyecto desarrollado con Flutter 2.8.1

  //* Esperamos por la inicialización de Flutter
  WidgetsFlutterBinding.ensureInitialized();
  
  //* Seteamos el estilo de las barras del sistema y status
  SystemChrome.setSystemUIOverlayStyle( SystemUiOverlayStyle(
    systemNavigationBarColor: Colors.grey[200],
    statusBarColor: Colors.grey[200],
    systemNavigationBarIconBrightness: Brightness.dark,
    statusBarIconBrightness: Brightness.dark,
    statusBarBrightness: Brightness.dark,
  ));

  //* Seteamos la orientación únicamente vértical
  SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp])
      .then((_) {
    runApp(const MyApp());
  });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //* Utilizo MultiProvider por defecto ya que es común disponer de varios providers en una aplicación. 
    
    //* Por otro lado he decidido probar la librería sizer para hacer los layouts responsive, anteriormente hacía este proceso de forma manual
    //* obteniendo el width y el height con la clase MediaQuery
    
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => LoginProvider())
      ],
      child: Sizer(
      builder: (context, orientation, deviceType) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          //* Uso este sistema de internacionalización ya que no quiero modificar la interfaz indicada para posibilitar el cambio manual
          localizationsDelegates: const [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: const [
            Locale('en', ''), 
            Locale('es', ''), 
          ],
          theme: ThemeData(fontFamily: 'Poppins'),
          home: const LoginPage(),
        );
      }
    ));
  }
}