
import 'package:flutter/cupertino.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:opendit_test/api/api_errors.dart';
import 'package:opendit_test/constants/colores.dart';
import 'package:opendit_test/models/login_response_model.dart';
import 'package:opendit_test/widgets/internal_snackbar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

//* Clase especifica para diferentes utilidades relacionadas con la api...
class ApiUtils {
  
  static Future<void> login(BuildContext context,String email, String password) async {
    
    var connectivityResult = await (Connectivity().checkConnectivity());
    
    if (connectivityResult == ConnectivityResult.none) {
      showTopSnackBar(
        context, 
        InternalSnackbar.message(
          message: AppLocalizations.of(context)!.noNetwork,
          icon: const Icon(CupertinoIcons.wifi_exclamationmark,color: Colores.rojo,),
        ),
        displayDuration: const Duration(seconds: 2)
      );
    } else{
      var url = Uri.parse('https://reqres.in/api/login');
      final response = await http.post(url, body: {'email': email, 'password': password});
      
      //? Podríamos controlar por statusCode para filtrar por 200,400,etc...
      //? En este caso tras haber analizado las respuestas del servidor me decanto por controlar solo el response
      //? ya que al obtener status 400 también obtengo información del error como detalle

      LoginResponseModel loginResponse = LoginResponseModel.fromJson(json.decode(response.body));
      if(loginResponse.token!=null){
        showTopSnackBar(
          context, 
          InternalSnackbar.message(
            message: AppLocalizations.of(context)!.loginOk,
            icon: const Icon(CupertinoIcons.check_mark_circled,color: Colores.verde,),
          ),
          displayDuration: const Duration(seconds: 2)
        );
      }else{
        String errorMessage = '';
        switch(loginResponse.error){
          case ApiErrors.userNotFound:
            errorMessage = AppLocalizations.of(context)!.userNotFound;
          break;
          case ApiErrors.missingEmailOrUsername:
            errorMessage = AppLocalizations.of(context)!.noEmail;
          break;
          case ApiErrors.missingPassword:
            errorMessage = AppLocalizations.of(context)!.noPassword;
          break;
        }
        showTopSnackBar(
          context, 
          InternalSnackbar.message(
            message: errorMessage,
            icon: const Icon(CupertinoIcons.clear_circled,color: Colores.rojo,),
          ),
          displayDuration: const Duration(seconds: 2)
        );
      }
    }
  }
}