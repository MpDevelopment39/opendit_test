//* Clase para definir todas las posibles respuestas de error de la api
class ApiErrors{
  //? Login Responses
  static const String userNotFound = "user not found";
  static const String missingPassword = "Missing password";
  static const String missingEmailOrUsername = "Missing email or username";
}