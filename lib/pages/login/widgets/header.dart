import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';

//* Clase con el diseño del Header de la pantalla de Login
class Header extends StatelessWidget {
  const Header({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: 7.h,
      ),
      child: Column(
        children: [
          SvgPicture.asset(
            'assets/images/logo.svg',
            height: 4.h,
          ),
          SizedBox(height: 5.h),
          SvgPicture.asset('assets/images/init.svg'),
        ],
      ),
    );
  }
}