import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:opendit_test/api/api_utils.dart';
import 'package:opendit_test/constants/colores.dart';
import 'package:opendit_test/providers/login_provider.dart';
import 'package:opendit_test/utils/utils.dart';
import 'package:opendit_test/widgets/internal_snackbar.dart';
import 'package:lottie/lottie.dart';
import 'package:sizer/sizer.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

//* Clase con el diseño y lógica del ModalBottom de la pantalla de Login
class ModalBottomLogin extends StatelessWidget {
  const ModalBottomLogin({
    Key? key,
    required this.formKey,
    required this.controllerEmail,
    required this.loginProvider,
    required this.controllerPassword,
  }) : super(key: key);

  final GlobalKey<FormState> formKey;
  final TextEditingController controllerEmail;
  final LoginProvider loginProvider;
  final TextEditingController controllerPassword;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.h,
      decoration: BoxDecoration(
          boxShadow: const [
            BoxShadow(
                color: Colors.black26,
                spreadRadius: 3,
                blurRadius: 5,
                offset: Offset(0, 3))
          ],
          color: Colores.blanco.withOpacity(0.9),
          borderRadius: const BorderRadius.vertical(
            top: Radius.circular(30),
          )),
      child: Form(
        key: formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 3.h),
              child: TextFormField(
                controller: controllerEmail,
                keyboardType: TextInputType.emailAddress,
                validator: (email) {
                  //*Validación sobre el formato del email
                  if (email!.isEmpty) {
                    loginProvider.showEmailError = true;
                    return AppLocalizations.of(context)!.emptyField;
                  } else if (!email.contains('@') || !email.contains('.')) {
                    loginProvider.showEmailError = true;
                    return AppLocalizations.of(context)!.invalidEmailFormat;
                  }
                },
                onChanged: (value) {
                  if (loginProvider.showEmailError) {
                    loginProvider.showEmailError = false;
                    loginProvider.showPasswordError = false;
                  }
                },
                style: TextStyle(fontSize: 14.sp, color: Colores.negro),
                decoration: InputDecoration(
                  suffixIcon: loginProvider.showEmailError
                      ? const Icon(
                          CupertinoIcons.info,
                          color: Colores.rojo,
                        )
                      : null,
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: const BorderSide(
                      color: Colores.azul,
                      width: 2.0,
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: const BorderSide(
                      color: Colores.gris,
                      width: 2.0,
                    ),
                  ),
                  errorMaxLines: 3,
                  errorStyle: const TextStyle(color: Colores.rojo),
                  contentPadding: EdgeInsets.symmetric(horizontal: 3.h),
                  label: Text(
                    AppLocalizations.of(context)!.emailLabel,
                    style: TextStyle(fontSize: 14.sp, color: Colores.negro),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(3.h, 20.0, 3.h, 0),
              child: TextFormField(
                controller: controllerPassword,
                //*Validación sobre el formato de la contraseña
                validator: (pass) {
                  if (pass!.isEmpty) {
                    loginProvider.showPasswordError = true;
                    return AppLocalizations.of(context)!.emptyField;
                  } else if (pass.length < 8) {
                    loginProvider.showPasswordError = true;
                    return AppLocalizations.of(context)!.invalidPasswordFormat1;
                  } else if (!Utils.validatePassword(pass)) {
                    loginProvider.showPasswordError = true;
                    return AppLocalizations.of(context)!.invalidPasswordFormat2;
                  }
                },
                onChanged: (value) {
                  if (loginProvider.showPasswordError) {
                    loginProvider.showEmailError = false;
                    loginProvider.showPasswordError = false;
                  }
                },
                style: TextStyle(fontSize: 14.sp, color: Colores.negro),
                obscureText: loginProvider.obscurePassword,
                decoration: InputDecoration(
                  suffixIcon: loginProvider.showPasswordError
                      ? IconButton(
                          onPressed: () {
                            loginProvider.obscurePassword =
                                !loginProvider.obscurePassword;
                          },
                          icon: const Icon(
                            CupertinoIcons.info,
                            color: Colores.rojo,
                          ))
                      : IconButton(
                          onPressed: () {
                            loginProvider.obscurePassword =
                                !loginProvider.obscurePassword;
                          },
                          icon: Icon(
                            loginProvider.obscurePassword
                                ? Icons.remove_red_eye_outlined
                                : Icons.remove_red_eye,
                            color: Colores.negro,
                          )),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: const BorderSide(
                      color: Colores.azul,
                      width: 2.0,
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: const BorderSide(
                      color: Colores.gris,
                      width: 2.0,
                    ),
                  ),
                  errorMaxLines: 2,
                  errorStyle: const TextStyle(color: Colores.rojo),
                  contentPadding: EdgeInsets.symmetric(horizontal: 3.h),
                  label: Text(
                    AppLocalizations.of(context)!.passwordLabel,
                    style: TextStyle(fontSize: 14.sp, color: Colores.negro),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, right: 30.0),
              child: Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                  onTap: () {
                    showTopSnackBar(
                        context,
                        InternalSnackbar.message(
                          message: AppLocalizations.of(context)!.functionDisabled,
                          icon: const Icon(
                            CupertinoIcons.clear_circled,
                            color: Colores.rojo,
                          ),
                        ),
                        displayDuration: const Duration(seconds: 2));
                  },
                  child: Text(
                    AppLocalizations.of(context)!.forgotMyPassword,
                    style: TextStyle(
                      fontSize: 11.sp,
                      decoration: TextDecoration.underline,
                      color: Colores.negro,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(3.h, 30.0, 3.h, 0.0),
              child: ElevatedButton(
                  onPressed: loginProvider.isLogin
                      ? null
                      : () async {
                        //*Hacemos una validación local del formato deseado para email y contraseña
                        //!Personalmente me gusta que este tipo de validaciones las realice la api directamente
                        //!Ya que si queremos actualizar las validaciones y están en la parte cliente hay que subir una nueva versión de la app
                        if (formKey.currentState!.validate()) {
                          FocusScopeNode currentFocus =
                              FocusScope.of(context);
                          if (!currentFocus.hasPrimaryFocus) {
                            currentFocus.unfocus();
                          }
                          loginProvider.isLogin = true;
                          await ApiUtils.login(context, controllerEmail.text,
                              controllerPassword.text);
                          loginProvider.isLogin = false;
                        }
                      },
                  style: ButtonStyle(
                    textStyle: MaterialStateProperty.all(
                      TextStyle(
                        fontSize: 15.sp,
                      ),
                    ),
                    backgroundColor: MaterialStateProperty.all(Colores.negro),
                    overlayColor: MaterialStateProperty.all(Colores.azul),
                    shadowColor: MaterialStateProperty.all(Colores.negro),
                    elevation: MaterialStateProperty.all(5),
                    minimumSize: MaterialStateProperty.all(const Size(350, 45)),
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  child: loginProvider.isLogin
                      ? Lottie.asset('assets/images/loading_blue.json',height: 30)
                      : Text(
                          AppLocalizations.of(context)!.initSession,
                          style: const TextStyle(
                            fontWeight: FontWeight.w600),
                        )),
            ),
            GestureDetector(
              onTap: () {
                showTopSnackBar(
                    context,
                    InternalSnackbar.message(
                      message: AppLocalizations.of(context)!.functionDisabled,
                      icon: const Icon(
                        CupertinoIcons.clear_circled,
                        color: Colores.rojo,
                      ),
                    ),
                    displayDuration: const Duration(seconds: 2));
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      AppLocalizations.of(context)!.registerText1,
                      style: TextStyle(
                        fontSize: 11.sp,
                        color: Colores.negro,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(width: 3),
                    Text(
                      AppLocalizations.of(context)!.registerText2,
                      style: TextStyle(
                        fontSize: 11.sp,
                        fontWeight: FontWeight.bold,
                        color: Colores.negro,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}