import 'package:flutter/material.dart';
import 'package:opendit_test/constants/colores.dart';
import 'package:opendit_test/providers/login_provider.dart';
import 'package:animate_do/animate_do.dart';
import 'widgets/header.dart';
import 'widgets/modal_bottom_login.dart';
import 'package:provider/provider.dart';

//* Clase de la pantalla principal de la aplicación
class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();

  final formKey = GlobalKey<FormState>();
  
  @override
  Widget build(BuildContext context) {
    final loginProvider = Provider.of<LoginProvider>(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colores.gris,
        body: SizedBox(
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              FadeInLeft(
                  delay: const Duration(milliseconds: 500),
                  child: const Header()),
              Positioned(
                child: FadeInUp(
                  delay: const Duration(milliseconds: 500),
                  child: ModalBottomLogin(
                      formKey: formKey,
                      controllerEmail: controllerEmail,
                      loginProvider: loginProvider,
                      controllerPassword: controllerPassword),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}