import 'package:flutter/cupertino.dart';

//* Clase para manejar el estado de la pantalla Login
class LoginProvider extends ChangeNotifier{
  
  bool? _showEmailError;
  bool? _showPasswordError;
  bool? _obscurePassword;
  bool? _isLogin;

  bool get showEmailError => _showEmailError ?? false;
  
  set showEmailError(bool showEmailError) {
    _showEmailError = showEmailError;
    notifyListeners();
  }

  bool get showPasswordError => _showPasswordError ?? false;
  
  set showPasswordError(bool showPasswordError) {
    _showPasswordError = showPasswordError;
    notifyListeners();
  }

  bool get obscurePassword => _obscurePassword ?? false;
  
  set obscurePassword(bool obscurePassword) {
    _obscurePassword = obscurePassword;
    notifyListeners();
  }

  bool get isLogin => _isLogin ?? false;
  
  set isLogin(bool isLogin) {
    _isLogin = isLogin;
    notifyListeners();
  }
}