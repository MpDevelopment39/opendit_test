import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:opendit_test/constants/colores.dart';

//* Clase reutilizable para mostrar Snackbars personalizados 
//* Su uso requiere la librería (top_snackbar_flutter) para visualizarlos en la parte superior de la pantalla 
class InternalSnackbar extends StatefulWidget {
  
  final String message;
  final Icon icon;
  final Color backgroundColor;
  final TextStyle textStyle;
  final List<BoxShadow> boxShadow;
  final BorderRadius borderRadius;

  // ignore: use_key_in_widget_constructors
  const InternalSnackbar.message({
    Key? key,
    required this.message,
    required this.icon,
    this.textStyle = const TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 13,
      color: Colors.white,
    ),
    this.backgroundColor = Colores.negro,
    this.boxShadow = _defaultBoxShadow,
    this.borderRadius = _defaultBorderRadius,
  });

  @override
  _InternalSnackbarState createState() => _InternalSnackbarState();
}

const _defaultBoxShadow = [
  BoxShadow(
    color: Colors.black26,
    offset: Offset(0.0, 8.0),
    spreadRadius: 1,
    blurRadius: 30,
  ),
];

const _defaultBorderRadius = BorderRadius.all(Radius.circular(12));


class _InternalSnackbarState extends State<InternalSnackbar> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      height: 7.h,
      width: 80.w,
      decoration: BoxDecoration(
        color: widget.backgroundColor,
        borderRadius: widget.borderRadius,
        boxShadow: widget.boxShadow,
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 3.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            widget.icon,
            const SizedBox(width: 10,),
            SizedBox(
              width: 65.w,
              child: Text(
                widget.message,
                style: theme.textTheme.bodyText2?.merge(
                  widget.textStyle,
                ),
                textAlign: TextAlign.start,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              ),
            ),
            const Spacer(),
            const Icon(
              CupertinoIcons.clear,
              color: Colors.grey
            ),  
          ],
        ),
      ),
    );
  }
}


