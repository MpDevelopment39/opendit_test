//* Clase general para diferentes utilidades dentro del ámbito global de la aplicación
class Utils {

  //* Función para comprobar la validez de la contraseña introducida
  static bool validatePassword(String passwordToValidate){
    String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = RegExp(pattern);
   return regExp.hasMatch(passwordToValidate);
  }

}