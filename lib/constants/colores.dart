import 'package:flutter/material.dart';

//* Clase de constantes para definir todos los colores que utilice la aplicación.
class Colores {
  static const Color azul = Color.fromRGBO(189, 255, 255, 1);
  static const Color rosa = Color.fromRGBO(255, 222, 224, 1);
  static const Color amarillo = Color.fromRGBO(252, 255, 191, 1);
  static const Color negro = Color.fromRGBO(0, 0, 0, 1);
  static const Color blanco = Color.fromRGBO(255, 255, 255, 1);
  static const Color gris = Color.fromRGBO(238, 238, 238, 1);
  static const Color rojo = Color.fromRGBO(255, 72, 73, 1);
  static const Color verde = Colors.green;
}